VCC= 3VOLTS
GND= GND	
SCK/SCL= D1
SDA/SDI= D2


----------------------------------------------------

C�DIGO

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11
#define BMP_CS 10

int altitude;

Adafruit_BMP280 bme; // I2C
//Adafruit_BMP280 bme(BMP_CS); // hardware SPI
//Adafruit_BMP280 bme(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

void setup() {
  Serial.begin(9600);
  Serial.println(F("Teste de temperatura/pressao/altitude"));

  if (!bme.begin(0x76)) {
    Serial.println("Nao foi achado um sensor BMP280 valido!");
    while (1);
  }
}

void loop() {

  // MEDE A TEMPERATURA
  Serial.print("Temperatura = ");
  Serial.print(bme.readTemperature());
  Serial.println(" C");


  // MEDE A PRESSAO
  Serial.print("Pressao = ");
  Serial.print(bme.readPressure() / 100);
  Serial.println(" mB");

  
  // MEDE A ALTITUDE APROXIMADA
  Serial.print("Altitude aproximada = ");
  Serial.print(bme.readAltitude(1022)); // esse � a altitude aproximada do seu local;
  Serial.println(" m");

  Serial.println();
  delay(20000);
}