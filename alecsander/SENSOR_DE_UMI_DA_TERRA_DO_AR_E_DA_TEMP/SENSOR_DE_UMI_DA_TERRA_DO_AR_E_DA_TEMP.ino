//dedfine entra do sensor dth11
#define pino_sinal_analogico A0
//bibliotecas
#include "DHT.h"
#include <LiquidCrystal.h>

//
#define DHTPIN 5
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
//portas onde e ligado o led
LiquidCrystal lcd(4, 0, 2, 14, 12, 13);

int valor_analogico;
float umidade, x;

void setup() {
  //liga lcd
  lcd.begin(16, 2);
  //define como entrada a onde o pino do dht vai
  pinMode(pino_sinal_analogico, INPUT);


  lcd.println("Sensor iniciado");



}


void loop() {

  //ler o valo recebido pelo sensor e transforma em porcentagem
  valor_analogico = analogRead(pino_sinal_analogico);
  x =  valor_analogico;
  umidade = (1 - x / 1024) * 100;


  float h = dht.readHumidity();
  float t = dht.readTemperature();

  //lcd setCursor= (0,0) = coluna e linha

  lcd.clear();
  lcd.setCursor(0, 0);


  lcd.print("Umidade: ");
  lcd.print(h);
  lcd.println(" %\t");
  lcd.setCursor(0, 1);
  lcd.print("Temp.: ");
  lcd.print(t);
  lcd.print(" C ");
  delay(2000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Umi. Terra: ");
  lcd.printf("%.0lf", umidade);
  lcd.print ("%");
  delay(2000);


}
