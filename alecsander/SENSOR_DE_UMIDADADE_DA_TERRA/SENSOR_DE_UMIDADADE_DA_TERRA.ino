#define pino_sinal_analogico A0
#include "DHT.h"
#define DHTPIN 5
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

int valor_analogico;
float umidade, x;

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(2000);
  pinMode(pino_sinal_analogico, INPUT);
  while (!Serial) { }

  Serial.println("Sensor iniciado");
  Serial.println("-------------------------------------");
  Serial.println("Rodando DHT!");
  Serial.println("-------------------------------------");



}

int timeSinceLastRead = 0;
void loop() {
  valor_analogico = analogRead(pino_sinal_analogico);
  x =  valor_analogico;
  umidade = (1 - x / 1024) * 100;

  if (timeSinceLastRead > 2000) {
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    if (isnan(h) || isnan(t) ) {
      Serial.println("Failed to read from DHT sensor!");
      timeSinceLastRead = 0;
      return;
    }
    
    Serial.print("Umidade: ");
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperatura: ");
    Serial.print(t);
    Serial.println(" *C ");

    Serial.print("umidade da terra: ");
    Serial.printf("%.0lf", umidade);
    Serial.println ("%");
    timeSinceLastRead = 0;
  }
   delay(100);
  timeSinceLastRead += 100;

  }
